## Root

### Check service

Ping service

```endpoint
GET /
```

#### Example request

```curl
curl http://<host>:<port>/
```

#### Example response

```json
OK
```



## Profile

Profile management API docs

### List profiles

```endpoint
GET /profile/
```

*Deprecated*!!

Will return 405 Error "Method not allowed"

### Create profile

Create new profile

```endpoint
POST /profile/
```

#### Example request

```curl
curl -X POST http://<host>:<port>/profile/ -d '<data json>'
```

#### Example request Body

```json
{
  "name": "Vasya",
  "value": 101 
}
```

Fields:

*name* - [Optional] any string

*value* - [options] any number

#### Example response

```json
{
  "id": "507f191e810c19729de860ea"
}
```

### Get profile

Get profile info

```endpoint
GET /profile/{id}/
```

#### Example request

```curl
curl http://<host>:<port>/profile/507f191e810c19729de860ea/
```

#### Example response

```json
{
  "id": "507f191e810c19729de860ea",
  "name": "Vasya",
  "value": 101
}
```

### Edit profile

```endpoint
POST /profile/{id}/
```

### Example request

```curl
curl -X POST http://<host>:<port>/profile/507f191e810c19729de860ea/ -d '<data json>'
```

#### Example request Body

```json
{
  "name": "Vasya Pupkin"
}
```

```json
{
  "value": 0
}
```

#### Example response

```json
{
  "id": "507f191e810c19729de860ea",
  "name": "Vasya Pupkin",
  "value": 0
}
```

