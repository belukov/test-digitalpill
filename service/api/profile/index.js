const express = require('express');
const router = express.Router();
const Err = require('../httperr');

const ProfileMdl = require('../../lib/models/profile');

router.param('profileId', (req, res, next, profileId) => {
 
  req.profileId = profileId;
  ProfileMdl.findById(profileId).then(doc => {
    if (!doc) return next(Err("Profile not found", 404));
    req.profileObj = doc;
    return next();
  }).catch(next);
});

router.get('/', (req, res, next) => {
  return next(Err("Method not allowed", 405));
});

router.post('/', async (req, res, next) => {
  let profile = new ProfileMdl(req.body);
  profile.save().then(() => {
    res.status(201);
    res.json({
      id: profile.id
    });
  }).catch(e => {
    let err = Err("Validation error", 400);
    err.errors = e.errors;
    return next(err);
  });
});

router.get('/:profileId/', (req, res, next) => {

  res.send(req.profileObj.toJSON());
});

router.post('/:profileId/', async (req, res, next) => {

  ["name", "value"].map(fld => {
    if (!(fld in req.body)) return;
    req.profileObj[fld] = req.body[fld];
  });
  if (!req.profileObj.isModified) {
    return next("Nothing to modify", 400);
  }
  req.profileObj.save().then(() => {
    res.send(req.profileObj.toJSON());
  }).catch(e => {

    let err = Err("Validation error", 400);
    err.errors = e.errors;
    return next(err);
  });

});

module.exports = router;
