const nconf = require('nconf');

nconf.argv()
  .env()
  .file('local', './config/local.json')
  .file('default', {
    file: './config/default.json'
  });

module.exports = nconf;
