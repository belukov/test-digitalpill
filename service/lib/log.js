const log4js = require('log4js');
const config = require('./config');


module.exports = function(loggerName) {
  let logger = log4js.getLogger(loggerName);
  logger.level = config.get('log_level') || 'info';
  return logger;
}
