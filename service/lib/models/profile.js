const mongoose = require('../mongoose');

const ProfileSchema = new mongoose.Schema({
  name: String,
  value: Number
}, {
  toJSON: {virtuals: true},
  toObject: {virtuals: true},
});

module.exports = mongoose.model('Profile', ProfileSchema);
