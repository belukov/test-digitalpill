const mongoose = require('mongoose');
const config = require('./config');
const log = require('./log')('db');

let db;

function connect() {

  let opts = config.get('db:options') || {};
  mongoose.connect(config.get('db:uri'), opts);
  db = mongoose.connection;
  db.on('error', e => {
    log.error('DB connection error', e);
  });
  db.once('open', function() {
    log.info('DB connected ', config.get('db:uri'));
  });
  db.on('disconnect', connect);

  process.on('SIGINT', function() {  
    db.close(function () { 
      console.log('Mongoose default connection disconnected through app termination'); 
      process.exit(0); 
    }); 
  }); 
}

connect();

module.exports = mongoose;

