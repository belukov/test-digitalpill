
const express = require('express');
const bodyParser = require('body-parser');
const config = require("./lib/config");
const log = require('./lib/log')('server');


const app = express();

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.use('*', (req, res, next) => {
  log.debug(req.method + ' ' + req.originalUrl);
  return next();
});

app.use('/', require('./api/'));

app.use((req, res, next) => {
  log.debug('page not fund ', req.originalUrl);
  let err = new Error('Not found');
  err.status = 404;
  return next(err);
});

app.use((err, req, res, next) => {
  log.warn('app error[%s]: ', err.status, err.message);
  res.status(err.status || 500);
  //res.send(err.message);
  res.json({
    message: err.message,
    errors: err.errors || {}
  })
});

if (module.parent) {
  module.exports = app;
} else {
  app.listen(config.get('port'), (err) => {
    if (err) {
      log.error("Can't listen: ", e);
    } else {
      log.info('API listen port ', config.get('port'));
    }
  });
}

