const mocha = require('mocha');
const assert = require('assert');
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../server');
let should = chai.should();

chai.use(chaiHttp);

describe("Test ping", () => {

  it("Must return OK on root", async () => {
  
    let res = await chai.request(server).get("/");
    res.should.have.status(200);
    res.text.should.be.a("String");
    assert.equal('OK', res.text);
  }); 

})
