const mocha = require('mocha');
const assert = require('assert');
const chai = require('chai');
const chaiHttp = require('chai-http');
const mongoose = require('../lib/mongoose');
const waitMongoose = require('./helpers/waitMongoose');
const server = require('../server');
let should = chai.should();

chai.use(chaiHttp);

describe("Test profile api", () => {

  let insertedId;
  let db;

  before(async () => {
  
    await waitMongoose();
    db = mongoose.connection.db;
    await db.dropDatabase();
  });

  it("Must return 405 err on profile root", async () => {
  
    let res = await chai.request(server).get("/profile/");
    res.should.have.status(405);

  });

  it("Must add profile ", async () => {
  
    let data = {
      name: "Vasya",
      value: 101
    }
    let res = await chai.request(server)
      .post("/profile/")
      .send(data);

    res.should.have.status(201);
    res.body.should.be.a('Object');
    res.body.should.to.have.nested.property('id');
    res.body.id.should.be.a("String");
    assert(res.body.id.length > 0, "Empty id in result");
    insertedId = res.body.id;
    //console.log("res id", insertedId);
  });

  it("Must get profile by id", async () => {
    assert(insertedId, "Must be saved id");

    let res = await chai.request(server).get('/profile/' + insertedId + '/');
    res.should.have.status(200);
    res.body.should.be.a('Object');
    //console.log('res', res.body);
    res.body.should.to.have.nested.property('id');
    res.body.should.to.have.nested.property('name');
    res.body.should.to.have.nested.property('value');
    assert.equal(insertedId, res.body.id);
    assert.equal("Vasya", res.body.name);
    assert.equal(101, res.body.value);
  });

  it("Must update only name", async () => {
    assert(insertedId, "Must be saved id");
    let res = await chai.request(server)
      .post(`/profile/${insertedId}/`)
      .send({
        name: "Vasya Pupkin"
      });
    res.should.have.status(200);
    res.body.should.be.a('Object');
    //console.log('res', res.body);
    res.body.should.to.have.nested.property('id');
    res.body.should.to.have.nested.property('name');
    res.body.should.to.have.nested.property('value');
    assert.equal(insertedId, res.body.id);
    assert.equal("Vasya Pupkin", res.body.name);
    assert.equal(101, res.body.value);
  });

 it("Must return 400 when value is not number", async () => {
    assert(insertedId, "Must be saved id");
    let res = await chai.request(server)
      .post(`/profile/${insertedId}/`)
      .send({
        value: "123asasa"
      });
    res.should.have.status(400);
    //console.log(res.body);
    assert.equal("Validation error", res.body.message);
    res.body.should.to.have.nested.property('errors.value.message');
  });


  it("Must add an empty profile", async () => {
  
    let res = await chai.request(server)
      .post("/profile/")
      .send({});

    res.should.have.status(201);
    res.body.should.be.a('Object');
    res.body.should.to.have.nested.property('id');
    res.body.id.should.be.a("String");
    assert(res.body.id.length > 0, "Empty id in result");
  });
});
