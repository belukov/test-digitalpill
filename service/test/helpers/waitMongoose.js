const mongoose = require('../../lib/mongoose');

const waitMongoose = () => {
  return new Promise(resolve => {
    if (mongoose.connection.db) return resolve();
      mongoose.connection.once('open', () => {
        return resolve();
      });
  });
}

module.exports = waitMongoose;

